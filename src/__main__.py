from constants.General import ROOT, EXTENSION
from data.data import params
from src.modules.backgroundExtraction import getBackgroundExtraction, getRefinedBackExtract
from src.modules.ellipticFrame import getEllipticFrame, getRefinedEllipticFrame
from src.modules.preview import getCentroidTrackers, getNumFrames, getStartIndex
from src.modules.watershedFrame import getWaterShedFrame, getSegmentedFrame
from utils.frameops import *
from utils.util import *

# -----------------------------------------------------------------

def __processImage(param: object, fileDir: str) -> UMat:

    uniformed: UMat = None
    mask: UMat = None
    refinedBgMask: UMat = None
    ellipticMask: UMat = None
    refEllipticMask: UMat = None
    segmentedMask: UMat = None
    waterShed: UMat = None

    # Substraction phase
    mask, uniformed = getBackgroundExtraction(param, fileDir)
    refinedBgMask = getRefinedBackExtract(param, mask)
    imshow("refined background", refinedBgMask)
    waitKey(1)

    # Elliptic phase
    ellipticMask = getEllipticFrame(param, refinedBgMask)
    refEllipticMask = getRefinedEllipticFrame(param, ellipticMask)
    imshow("refined elliptic", refEllipticMask)
    waitKey(1)

    waterShed = getWaterShedFrame(param, uniformed)
    imshow("watershed", waterShed)
    waitKey(1)

    segmentedMask = getSegmentedFrame(param, refEllipticMask, waterShed)
    imshow("segmented", segmentedMask)
    waitKey(1)
    return segmentedMask

# -----------------------------------------------------------------

def __getDataOf(enviroment: str) -> (object, int, int):

    parameter: dict = {}
    numFrames: int = 0
    startIndex: int = 0

    parameter = params[enviroment]
    numFrames = getNumFrames(enviroment)
    startIndex = getStartIndex(enviroment)

    return parameter, numFrames, startIndex

# -----------------------------------------------------------------

def __getImageRoute(index, startIndex, env) -> str:

    frameName: str = ""
    fileDir: str = ""

    frameName = '/frame_' + str(index + startIndex)
    fileDir = ROOT + env + frameName + EXTENSION

    return fileDir

# -----------------------------------------------------------------

def main():

    enviroment: str = ""
    fileDir: str = ""
    frameName: str = ""

    numFrames: int = 0
    startIndex: int = 0

    parameter: dict = {}
    ct1: CentroidTracker = None
    ct2: CentroidTracker = None
    processed: UMat = None

    enviroment = "smr1"

    parameter, numFrames, startIndex = __getDataOf(enviroment)
    ct1, ct2 = getCentroidTrackers(parameter)

    for index in range(numFrames):

        dirFolderCreated = createDirectory(index)

        fileDir = __getImageRoute(index, startIndex, enviroment)

        processed = __processImage(parameter, fileDir)
        circleObjectsIn(parameter, processed, ct1, ct2, fileDir)

        generateResume(dirFolderCreated)

    printAccuracy(parameter, ct1, ct2)

# -----------------------------------------------------------------

if __name__ == "__main__":

    main()

# -----------------------------------------------------------------
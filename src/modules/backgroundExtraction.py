import cv2
from cv2.cv2 import UMat
from utils.filters import uniformImage, filterBlobsByLength, ellipticFilter
from utils.frameops import getFrame, getGrayScaleFrame, extractBackground, binarize

#--------------------------------------------------------------------

def getBackgroundExtraction(param, fileDir) -> (UMat, UMat):

    original: UMat
    uniformed: UMat
    gray: UMat

    original = getFrame(fileDir, param["dim"])
    uniformed = uniformed2 = uniformImage(original)
    gray = getGrayScaleFrame(uniformed, 1)
    fgMask = extractBackground(gray)

    return fgMask, uniformed2

#--------------------------------------------------------------------

def getRefinedBackExtract(param, fgMask):

    binary: UMat
    filtered: UMat

    binary = binarize(fgMask)
    filtered = filterBlobsByLength(binary, param["fil1"])  # Se eliminan los blobs con menos de 180 pixeles
    # frameTotal = fgMask

    return filtered

#--------------------------------------------------------------------
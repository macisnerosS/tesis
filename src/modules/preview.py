import glob

from classes.centroidTracker import CentroidTracker
from constants.General import ROOT, EXTENSION


# -----------------------------------------------------------------

def getCentroidTrackers (param: object) -> object:

    ct1: CentroidTracker
    ct2: CentroidTracker

    ct1 = CentroidTracker("E", param["iddle"])
    ct2 = CentroidTracker("A", param["iddle"])

    return ct1, ct2

# -----------------------------------------------------------------

def getNumFrames (env: str) -> int:

    numFrames: int

    dir: str = ""

    dir = ROOT + env + "/*" + EXTENSION
    numFrames = len(glob.glob(dir))

    return numFrames

# -----------------------------------------------------------------

def getStartIndex(env: str) -> int:

    startIndex: int
    firstNameFile: int

    dir: str

    # Gets first image name
    dir = ROOT + env + "/*" + EXTENSION
    firstNameFile = glob.glob(dir)[0]

    # Gets index of first image
    startIndex = firstNameFile.split("_")[-1].replace(EXTENSION, "")
    startIndex = int(startIndex)

    return startIndex

# -----------------------------------------------------------------
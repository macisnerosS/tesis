
import numpy as np

from cv2.cv2 import dilate, subtract, imshow, UMat

from utils.filters import filterBlobsByLength
from utils.frameops import segmentWatershed, binarize

# --------------------------------------------------------------------

def getWaterShedFrame(param, frame):

    wsd1: UMat
    wsd2: UMat
    wsd3: UMat

    # Gets countour at three diferent levels
    wsd1 = __calculateWaterShed(frame, 0.2, param["wsd"])
    wsd2 = __calculateWaterShed(frame, 0.5, param["wsd"])
    wsd3 = __calculateWaterShed(frame, 0.7, param["wsd"])

    return wsd1 + wsd2 + wsd3

# --------------------------------------------------------------------

def getSegmentedFrame(param, mask, wsd):

    splitted: UMat
    binary: UMat
    filtered: UMat

    # Splits blobs
    splitted = subtract(mask, wsd)

    binary = binarize(splitted, thresh=254)
    filtered = filterBlobsByLength(binary, param["fil3"])

    # imshow("mascara", filtered)

    return filtered

# --------------------------------------------------------------------

def __calculateWaterShed(frame, percentage, height):

    wsd: UMat
    dilated: UMat

    # Gets people's contour
    wsd = segmentWatershed(frame, percentage)
    dilated = dilate(wsd, kernel=np.ones(height, np.uint8), iterations=1)  # dilata los bordes

    return dilated

# --------------------------------------------------------------------
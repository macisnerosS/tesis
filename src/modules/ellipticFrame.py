from cv2.cv2 import UMat

from utils.frameops import binarize
from utils.filters import ellipticFilter, filterBlobsByLength

#--------------------------------------------------------------------

def getEllipticFrame(param, mask):

    elliptic: UMat
    binary: UMat
    filtered: UMat

    elliptic = ellipticFilter(mask, *param["elip1"])
    binary = binarize(elliptic, thresh=param["bin1"])
    filtered = filterBlobsByLength(binary, param["fil2"])

    return filtered

#--------------------------------------------------------------------

def getRefinedEllipticFrame(param, mask):

    elliptic: UMat
    binary: UMat

    elliptic = ellipticFilter(mask, *param["elip2"])
    binary = binarize(elliptic, thresh=param["bin2"])

    return binary

#--------------------------------------------------------------------
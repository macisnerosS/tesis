import shutil

import numpy as np
from cv2 import *
from skimage.measure import label, regionprops

from classes.centroidTracker import CentroidTracker

# -----------------------------------------------------------------
from constants.General import RESULT, EXTENSION

stages = {1: "Imagen original", 2: "Imagen uniformizada", 3: "Escala de grises", 4: "Extracion de background",
          5: "Primera binarizacion", 6: "Elimina blobs menores 180", 7: "Primer filtro eliptico", 8: "Segunda binarizacion",
          9: "Elimina blobs menores 120", 10: "Watershed", 11: "Segundo filtro eliptico", 12: "Tercera binarizacion",
          13: "Elimina blobs menores 130", 14: "Resultados", 15: "NoName", 16: "NoName", 17: "NoName", 18: "NoName",
          19:"NoName", 20: "NoName",21: "NoName",22: "NoName",23: "NoName",24: "NoName",25: "NoName",26: "NoName"}

iterStage = iter(range(1, len(stages) + 1))

imagesResume = []  # Contiene las imagenes del resumen

numCase = 0  # Numero de caso del directorio

# -----------------------------------------------------------------

# Genera una carpeta con los resultados de
# todos los casos seleccionados
def createDirectory(numDirectory) -> str:

    global numCase

    numCase = numDirectory + 1

    dirFolderCreated = RESULT + "Caso_" + str(numCase)

    # Checks if folder was created early
    if os.path.exists(dirFolderCreated):
        shutil.rmtree(dirFolderCreated)

    os.mkdir(dirFolderCreated)

    return dirFolderCreated

# -----------------------------------------------------------------

def writeImage(frame: UMat, addResume=True):
    global iterStage, numCase, imagesResume
    dirFolderCreated = RESULT + "Caso_" + str(numCase)
    indexStage = next(iterStage)
    imwrite(dirFolderCreated + "/S" + str(numCase) + "_" + str(indexStage) + " - " + stages[indexStage] + EXTENSION, frame)
    # if addResume:
    #     imagesResume.append(frame)
    pass

# -----------------------------------------------------------------

def generateResume(dirFolderCreated):

    global imagesResume, iterStage, numCase

    # stop = len(imagesResume) // 2
    # upper = hconcat(imagesResume[:stop - 1])
    # down = hconcat(imagesResume[stop-1:2 * stop -2])
    # print(len(imagesResume[:stop]), len(imagesResume[stop:]))
    # imwrite(dirFolderCreated + "/Resumen_" + str(numCase) + ".jpg", vconcat([upper, down]))
    imagesResume = []
    iterStage = iter(range(1, len(stages) + 1))

# -----------------------------------------------------------------

def printAccuracy(param: object, ct: CentroidTracker,ct2: CentroidTracker):

    inputs: int
    outputs: int
    inputs2: int
    outputs2: int

    inputs, outputs = ct.getDataFlow()
    inputs2, outputs2 = ct2.getDataFlow()

    inputAccur = round((inputs2/param["realInput"])*100, 2)
    outputAccur = round((outputs/param["realOutput"])*100, 2)

    print(f'Input accuracy: {inputAccur}%, Output accuracy: {outputAccur}%')

# -----------------------------------------------------------------
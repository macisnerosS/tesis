import cv2

# -----------------------------------------------------------------

from constants.General import ROOT
from constants.General import EXTENSION

# -----------------------------------------------------------------

def main():

    arrEnv: []
    arrVideoName: []

    arrEnv = ["smr6", "smr7", "smr8", "smr9", "smr10", "smr11"]

    arrVideoName = [
        "VID_20191125_171158",
        "VID_20191125_171733",
        "VID_20191125_173531",
        "VID_20191125_172051",
        "VID_20191125_172624",
        "VID_20191125_170520"
    ]

    for strEnv, strVideoName in zip(arrEnv, arrVideoName):

        vidcap = cv2.VideoCapture(ROOT + strVideoName + '.mp4')
        success, img = vidcap.read()

        intCount = 0

        while success:

            cv2.imwrite(ROOT + strEnv + f"/frame_{intCount}{EXTENSION}", img)
            success, img = vidcap.read()

            intCount += 1

# -----------------------------------------------------------------

if __name__ == '__main__':

    main()

# -----------------------------------------------------------------
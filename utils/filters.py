from cv2 import *
import numpy as np
import scipy.ndimage.filters as fi
from utils.util import writeImage

# -----------------------------------------------------------------

def filterBlobsByLength(img, min_size):

    # find all your connected components (white blobs in your image)
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(img, connectivity=8)

    # connectedComponentswithStats yields every seperated component with information on each of them, such as size
    # the following part is just taking out the background which is also considered a component, but most of the time we don't want that.
    sizes = stats[1:, -1]
    nb_components = nb_components - 1

    # minimum size of particles we want to keep (number of pixels)
    # here, it's a fixed value, but you can set it as you want, eg the mean of the sizes or whatever

    # your answer image
    img2 = np.zeros((output.shape), dtype=np.uint8)

    # for every component in the image, you keep it only if it's above min_size
    for i in range(0, nb_components):

        if sizes[i] >= min_size:

            img2[output == i + 1] = 255

    writeImage(img2)

    return img2

# -----------------------------------------------------------------

def ellipticFilter(fgMask: UMat, rows: int, cols: int, sigma) -> UMat:

    # Genera matriz de ceros
    inp = np.zeros((rows, cols))

    # Genera el kernel con un filtro gaussiano
    inp[rows // 2, cols // 2] = 1
    image = cv2.filter2D(fgMask, kernel=fi.gaussian_filter(inp, sigma), ddepth=CV_8U)

    writeImage(image)

    return image

# -----------------------------------------------------------------

def diskFilter(fgMask: cv2.UMat) -> UMat:

    kernel = getStructuringElement(MORPH_ELLIPSE, (5, 5))
    kernel[1, 0], kernel[1, 4], kernel[3, 0], kernel[3, 4] = 0, 0, 0, 0
    image = dilate(src=fgMask, kernel=kernel, iterations=1)

    writeImage(image)

    return image

# -----------------------------------------------------------------

def erodeImage(fgMask: UMat) -> UMat:

    kernel = getStructuringElement(MORPH_ELLIPSE, (3, 3))
    fgMask = dilate(src=fgMask, kernel=kernel, iterations=2)
    image = erode(src=fgMask, kernel=kernel, iterations=4)

    writeImage(image)

    return image

# -----------------------------------------------------------------

# Aplica filtro de mediana 3 veces para uniformizar
def uniformImage(frame):

    for x in range(3):

        frame = medianBlur(frame, 5)

    writeImage(frame)

    return frame

# -----------------------------------------------------------------

# def segmentationHPF(frame):
#     blur2 = cv2.blur(frame, (11, 11))  # se obtiene la media
#     difference = frame - blur2
#     highpass = difference + frame
#     highpass = cvtColor(highpass, COLOR_RGB2GRAY)
#     _, binary = threshold(highpass, 60, 255, THRESH_BINARY)
#     binary = filterBlobsByLength(binary, 60)
#     # binary = cv2.dilate(binary, np.ones((3, 3), np.uint8))
#     imshow("binary", binary)
#     return binary
#
# -----------------------------------------------------------------

def segmentationLaplacian(frame):

    # Remove noise by blurring with a Gaussian filter
    src = cv2.GaussianBlur(frame, (3, 3), 0)

    src_gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)

    # Apply Laplace function
    dst = cv2.Laplacian(src_gray, CV_8U, ksize=3)
    _, binary = threshold(dst, 10, 255, THRESH_BINARY)
    imshow("laplace", binary)
    binary = GaussianBlur(binary, (5,5), sigmaX=1)
    _, binary = threshold(binary, 150, 255, THRESH_BINARY)
    imshow("gausiano", binary)
    binary = filterBlobsByLength(binary, 50)
    # binary = cv2.dilate(binary, np.ones((2, 2), np.uint8), iterations=1)
    abs_dst = cv2.convertScaleAbs(binary)
    imshow("laplaciano", abs_dst)
    return abs_dst

# -----------------------------------------------------------------
from constants.General import RESULT, FRAME_SIZE, EXTENSION
import numpy as np
import cv2

# -----------------------------------------------------------------

WINDOWS = 20
MAXFRAMES = 2000

# -----------------------------------------------------------------

def __generateBackgrounds(backgrounds: []):

    frames: []
    medianFrame: []
    intervalName: str
    imageName: str

    for index, frameIds in enumerate(backgrounds):

        for fid in frameIds:

            filename = RESULT + ("seq_{:0>6d}" + EXTENSION).format(fid + 1)

            frame = cv2.imread(filename)
            frame = cv2.resize(frame, FRAME_SIZE, interpolation=cv2.INTER_LINEAR)
            frames.append(frame)

        medianFrame = np.median(frames, axis=0).astype(dtype=np.uint8)

        intervalName = str(frameIds[0]) + "_" + str(frameIds[-1])
        imageName = RESULT + "background_" + intervalName

        cv2.imwrite(imageName + EXTENSION, medianFrame)

# -----------------------------------------------------------------

def __selectFrames(points):

    backgrounds: []

    for point in points:

        if point + WINDOWS <= MAXFRAMES:

            backgrounds.append(range(point, point + WINDOWS + 1))

    return backgrounds

# -----------------------------------------------------------------

# Generates background using reference frames
def main():

    referenceFrames: []
    backgrounds: []

    referenceFrames = [0, 1200, 1400, 1899]

    backgrounds = __selectFrames(referenceFrames)

    __generateBackgrounds(backgrounds)

# -----------------------------------------------------------------

if __name__ == "__main__":

    main()

# -----------------------------------------------------------------
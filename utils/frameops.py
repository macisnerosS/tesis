from collections import OrderedDict

import numpy as np
from cv2 import *
from cv2 import createBackgroundSubtractorKNN as Substractor
from skimage.measure import regionprops, label

from constants.General import BLUE, GREEN, RED, FRAME_SIZE
from utils.util import writeImage

# -----------------------------------------------------------------

backSub = Substractor(history=1000, dist2Threshold=200, detectShadows=True)

# -----------------------------------------------------------------

def circleObjectsIn (param: object, fgMask: UMat, ct, ct2, filename) -> UMat:

    global numCase

    centroids: [] = []
    frame: UMat = None

    __getCentroids(fgMask, centroids)

    frame = getFrame(filename, param["dim"])

    __setLimitsnShapes(param, frame, ct, ct2)
    __drawGraphics(param, frame, ct, ct2, centroids)

    # writeImage(frame)
    # imwrite("C:/Users/Miguel Cisneros/Desktop/Resultados/circle_" + str(numCase) + ".jpg", frame)

# -----------------------------------------------------------------

def getGrayScaleFrame (frame: UMat, index) -> UMat:
    # frameGray = cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frameGray = frame[:, :, index]

    writeImage(frameGray)

    return frameGray

# -----------------------------------------------------------------

def extractBackground (grayScale: UMat) -> UMat:

    global backSub

    fgMask: UMat = None

    fgMask = backSub.apply(grayScale)

    writeImage(fgMask)

    return fgMask

# -----------------------------------------------------------------

def getFrame(filename, dimension) -> UMat:

    # Obtiene el cuadro en el tamaño
    frame = imread(filename)
    frame = resize(frame, FRAME_SIZE)

    writeImage(frame[dimension[0]:, dimension[1]:dimension[2], :], addResume=False)

    return frame[dimension[0]:, dimension[1]:dimension[2], :]

# -----------------------------------------------------------------

def binarize(fgMask, thresh=127) -> UMat:

    _, fgMask = threshold(fgMask, thresh, 255, THRESH_BINARY)

    writeImage(fgMask)

    return fgMask

# -----------------------------------------------------------------

def segmentWatershed(frame: UMat, percentage) -> UMat:

    copyframe = frame

    gray = copyframe[:, :, 1]
    ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # noise removal
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)

    # sure background area
    sure_bg = cv2.dilate(opening, kernel, iterations=3)

    # Finding sure foreground area
    dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
    ret, sure_fg = cv2.threshold(dist_transform, percentage * dist_transform.max(), 255, 0)

    # Finding unknown region
    sure_fg = np.uint8(sure_fg)
    unknown = cv2.subtract(sure_bg, sure_fg)

    # Marker labelling
    ret, markers = cv2.connectedComponents(sure_fg)

    # Add one to all labels so that sure background is not 0, but 1
    markers = markers + 1

    # Now, mark the region of unknown with zero
    markers[unknown == 255] = 0
    markers = cv2.watershed(copyframe, markers)
    copyframe[markers == -1] = [255, 255, 255]
    gray2 = cv2.cvtColor(copyframe, cv2.COLOR_BGR2GRAY)

    return binarize(gray2, thresh=254)

# -----------------------------------------------------------------

def __drawGraphics(param, frame, ct, ct2, centroids):

    inputs: int
    outputs: int

    objects: OrderedDict
    objects2: OrderedDict

    objects, _, outputs = ct.update(centroids)
    __drawTracker(frame, 'OUTPUT: ' + str(outputs), (5, 30), BLUE, 1 - param["down"], objects)

    objects2, inputs, _ = ct2.update(centroids)
    __drawTracker(frame, 'INPUT: ' + str(inputs), (5, 15), GREEN, param["upper"], objects2)

# -----------------------------------------------------------------

def __getCentroids(mask: UMat, centroids:[]):

    label_img: UMat
    properties: []

    # Gets properties of blobs
    label_img = label(mask)
    properties = regionprops(label_img)

    # Adds centroids of each blob
    for prop in properties:

        xcentroid: int
        ycentroid: int

        xcentroid = int(prop.centroid[1])
        ycentroid = int(prop.centroid[0])

        centroids.append((xcentroid, ycentroid))

# -----------------------------------------------------------------

def __setLimitsnShapes(param, frame, ct, ct2):

    __setLimitnShapeTracker(param, frame, ct)
    __setLimitnShapeTracker(param, frame, ct2)

# -----------------------------------------------------------------

def __setLimitnShapeTracker(param: object, frame: UMat, ct):

    ct.setLimits(param["upper"], param["down"])
    ct.setShapeFrame(frame.shape[0], frame.shape[1])

# -----------------------------------------------------------------

def __drawTracker(frame: UMat, msg, pos, color, param, objects):

    limit: int

    cv2.putText(frame, msg, pos, cv2.FONT_HERSHEY_SIMPLEX, 0.4, GREEN, 1)

    limit = int(frame.shape[0] * param)
    cv2.line(frame, (0, limit), (frame.shape[1] - 1, limit), RED)

    for (objectID, centroid) in objects.items():
        cv2.circle(frame, (centroid[0], centroid[1]), 4, color, -1)

    imshow("circle objects", frame)

# -----------------------------------------------------------------
# import the necessary packages
from imutils.video import VideoStream
import numpy as np
import argparse
import imutils
import time
import cv2
import os

def function(frame):
    LABELS = open(labelsPath).read().strip().split("\n")

    # grab the frame from the threaded video stream
    # resize the frame to have a width of 600 pixels (while
    # maintaining the aspect ratio), and then grab the image
    # dimensions
    frame = imutils.resize(frame, width=600)
    (H, W) = frame.shape[:2]

    # sort the indexes of the bounding boxes in by their corresponding
    # prediction probability (in descending order)
    idxs = np.argsort(boxes[0, 0, :, 2])[::-1]

    # initialize the mask, ROI, and coordinates of the person for the
    # current frame
    mask = None
    roi = None
    coords = None

    # loop over the indexes
    for i in idxs:
        # extract the class ID of the detection along with the
        # confidence (i.e., probability) associated with the
        # prediction
        classID = int(boxes[0, 0, i, 1]) # ya lo tengo
        confidence = boxes[0, 0, i, 2] # porcentaje de confianza

        # if the detection is not the 'person' class, ignore it
        if LABELS[classID] != "person":
            continue

        # filter out weak predictions by ensuring the detected
        # probability is greater than the minimum probability
        if confidence > args["confidence"]:
            # scale the bounding box coordinates back relative to the
            # size of the image and then compute the width and the
            # height of the bounding box
            box = boxes[0, 0, i, 3:7] * np.array([W, H, W, H])
            (startX, startY, endX, endY) = box.astype("int")
            coords = (startX, startY, endX, endY)
            boxW = endX - startX
            boxH = endY - startY

            # extract the pixel-wise segmentation for the object,
            # resize the mask such that it's the same dimensions of
            # the bounding box, and then finally threshold to create
            # a *binary* mask
            mask = masks[i, classID]
            mask = cv2.resize(mask, (boxW, boxH),
                              interpolation=cv2.INTER_NEAREST)
            mask = (mask > args["threshold"])

            # extract the ROI and break from the loop (since we make
            # the assumption there is only *one* person in the frame
            # who is also the person with the highest prediction
            # confidence)
            roi = frame[startY:endY, startX:endX][mask]
            break

    # initialize our output frame
    output = frame.copy()

    # if the mask is not None *and* we are in privacy mode, then we
    # know we can apply the mask and ROI to the output image
    if mask is not None and privacy:
        # blur the output frame
        output = cv2.GaussianBlur(output, K, 0)

        # add the ROI to the output frame for only the masked region
        (startX, startY, endX, endY) = coords
        output[startY:endY, startX:endX][mask] = roi

    # show the output frame
    cv2.imshow("Video Call", output)
    key = cv2.waitKey(1) & 0xFF
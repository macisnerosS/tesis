
from scipy.spatial import distance as dist
from collections import OrderedDict
import numpy as np

# -----------------------------------------------------------------

class CentroidTracker():

    def __init__(self, sector, maxTimeDisappeared=1.67):

        self.nextObjectID = 0
        self.objects = OrderedDict()
        self.disappeared = OrderedDict()
        self.states = OrderedDict()
        self.height = 0
        self.width = 0
        self.inputs = 0
        self.outputs = 0
        self.upperLimit = 0
        self.downLimit = 0
        self.sector = sector
        self.maxDisappeared = int(maxTimeDisappeared * 30)

    # -----------------------------------------------------------------

    def inside(self, letter, centroid):

        if letter == "A":

            return centroid[1] > self.height * self.downLimit

        elif letter == "P":

            return self.height * self.upperLimit < centroid[1] < self.height * self.downLimit

        elif letter == "E":

            return centroid[1] < self.height * self.upperLimit

    # -----------------------------------------------------------------

    def getDataFlow(self):

        return self.inputs, self.outputs

    # -----------------------------------------------------------------

    def setLimits(self, upperLimit, downLimit):

        self.upperLimit = upperLimit
        self.downLimit = 1 - downLimit

    # -----------------------------------------------------------------

    def getStates(self):

        return self.states

    # -----------------------------------------------------------------

    def setShapeFrame(self, height, width):

        self.height = height
        self.width = width

    # -----------------------------------------------------------------

    def updateState(self, ObjectID, centroid):

        if self.inside("E", centroid):

            isCrossedAnP = self.states[ObjectID] == ["A","P",""]

            if isCrossedAnP and self.sector == "A":

                self.inputs += 1

            self.states[ObjectID] = ["", "", "E"]

        elif self.inside("A", centroid):

            isCrossedPnE: bool = self.states[ObjectID] == ["","P","E"]

            if isCrossedPnE and self.sector == "E":

                self.outputs += 1

            self.states[ObjectID] = ["A", "", ""]

        elif self.inside("P", centroid):

            self.states[ObjectID][1] = "P"

    # -----------------------------------------------------------------

    def register(self, centroid):

        self.objects[self.nextObjectID] = centroid
        self.disappeared[self.nextObjectID] = 0

        if self.inside("A", centroid):

            self.states[self.nextObjectID] = ["A", "", ""]

        else:

            self.states[self.nextObjectID] = ["", "", "E"]

        self.nextObjectID += 1

    # -----------------------------------------------------------------

    def deregister(self, objectID):

        del self.objects[objectID]
        del self.disappeared[objectID]
        del self.states[objectID]

    # -----------------------------------------------------------------

    def update(self, centroids):

        isRegisterEmpty: bool = len(centroids) == 0

        if isRegisterEmpty:

            for objectID in list(self.disappeared.keys()):

                if self.disappeared[objectID] > self.maxDisappeared:

                    self.deregister(objectID)

            return (self.objects, self.inputs, self.outputs)

        inputCentroids = np.zeros((len(centroids), 2), dtype="int")

        for (i, (xcentroid, ycentroid)) in enumerate(centroids):

            inputCentroids[i] = (xcentroid, ycentroid)

        noObjectsRegistered: bool = len(self.objects) == 0

        if noObjectsRegistered:

            for i in range(0, len(inputCentroids)):

                if self.inside(self.sector, inputCentroids[i]):

                    self.register(inputCentroids[i])

        else:

            objectIDs = list(self.objects.keys())
            objectCentroids = list(self.objects.values())
            D = dist.cdist(np.array(objectCentroids), inputCentroids)
            rows = D.min(axis=1).argsort()
            cols = D.argmin(axis=1)[rows]

            usedRows = set()
            usedCols = set()

            for (row, col) in zip(rows, cols):

                if row in usedRows or col in usedCols:

                    continue

                objectID = objectIDs[row]

                self.__updateCentroidPosition(objectID, inputCentroids, col)

                usedRows.add(row)
                usedCols.add(col)

            unusedRows = set(range(0, D.shape[0])).difference(usedRows)
            unusedCols = set(range(0, D.shape[1])).difference(usedCols)

            if D.shape[0] >= D.shape[1]:

                for row in unusedRows:

                    objectID = objectIDs[row]
                    self.disappeared[objectID] += 1

                    if self.disappeared[objectID] > self.maxDisappeared:

                        self.deregister(objectID)
            else:

                for col in unusedCols:

                    if self.inside(self.sector,inputCentroids[col]):

                        self.register(inputCentroids[col])

            self.__verifyJumpExtreme2Extreme()

        return (self.objects, self.inputs, self.outputs)

    # -----------------------------------------------------------------

    def __distance(self, x, y):

        return ((x[1]-y[1])**2 + (x[0]-y[0])**2)**0.5

    # -----------------------------------------------------------------

    def __holdPosition(self, objectID, currCentPosition):

        self.objects[objectID] = currCentPosition
        self.disappeared[objectID] = 0

    # -----------------------------------------------------------------

    def __verifyJumpExtreme2Extreme(self):

        for key, value in zip(list(self.objects.keys()), list(self.objects.values())):

            self.updateState(key, value)

            isCentInsideE: bool = self.inside("E", self.objects[key])
            isCentInsideA: bool = self.inside("A", self.objects[key])

            if self.sector == "A" and isCentInsideE:

                self.deregister(key)

            elif self.sector == "E" and isCentInsideA:

                self.deregister(key)

    # -----------------------------------------------------------------

    def __updateCentroidPosition(self, objectID, inputCentroids, col):

        lastCentPosition = self.objects[objectID]
        currCentPosition = inputCentroids[col]
        isNewPosNear = (self.__distance(currCentPosition, self.objects[objectID]) < 25)

        if self.inside("A", currCentPosition) and self.inside("A", lastCentPosition):

            self.__holdPosition(objectID, currCentPosition)

        elif self.inside("E", currCentPosition) and self.inside("E", lastCentPosition):

            self.__holdPosition(objectID, currCentPosition)

        elif isNewPosNear:

            self.__moveCentroidInsideRange(objectID, inputCentroids[col], currCentPosition, lastCentPosition)

    # -----------------------------------------------------------------

    def __moveCentroidInsideRange(self, objectID,inputCentroid, currCentPosition, lastCentPosition):

        isObjGoDown = (self.states[objectID] == ["A", "P", ""])
        isObjGoUp = (self.states[objectID] == ["", "P", "E"])
        isNotMovingDown = (self.objects[objectID][1] < inputCentroid[1])
        isNotMovingUp = (self.objects[objectID][1] > inputCentroid[1])

        if self.inside("P", currCentPosition) and self.inside("A", lastCentPosition) and isObjGoDown:

            self.__holdPosition(objectID, currCentPosition)

        elif self.inside("P", currCentPosition) and self.inside("E", lastCentPosition) and isObjGoUp:

            self.__holdPosition(objectID, currCentPosition)

        else:

            self.__holdPosition(objectID, currCentPosition)

        if self.inside("P", currCentPosition) and isObjGoDown and isNotMovingDown:

            self.disappeared[objectID] += 1

        elif self.inside("P", currCentPosition) and isObjGoUp and isNotMovingUp:

            self.disappeared[objectID] += 1

    # -----------------------------------------------------------------